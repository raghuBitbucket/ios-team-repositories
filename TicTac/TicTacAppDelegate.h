//
//  TicTacAppDelegate.h
//  TicTac
//
//  Created by bijugopi on 12/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TicTacViewController;

@interface TicTacAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet TicTacViewController *viewController;

@end
