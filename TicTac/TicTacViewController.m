//
//  TicTacViewController.m
//  TicTac
//
//  Created by bijugopi on 12/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TicTacViewController.h"

@implementation TicTacViewController
@synthesize Img1,Img8,Img9,Img7,Img6,Img5,Img4,Img3,Img2;
@synthesize board,tictactoe,NewGameButton;
@synthesize oImg,xImg;
@synthesize placed;
//@synthesize ImgViewsByName;

NSMutableDictionary *ImgView;
- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

-(void)viewDidLoad
{
    self.view.backgroundColor=[UIColor whiteColor];
    
    oImg = [UIImage imageNamed:@"O.png"];
    xImg = [UIImage imageNamed:@"X.png"];
    
    tictactoe = [UIImage imageNamed:@"board.png"];
    
    CGRect frame = CGRectMake(0, 0, 320, 320);
    board = [[UIImageView alloc]initWithFrame:frame];
    [board setImage:tictactoe];
    [self.view addSubview:board];
    
    //1st row
    CGRect f1=CGRectMake(10, 5, 80, 80);
    Img1 = [[UIImageView alloc]initWithFrame:f1];
    [Img1 setBounds:f1];

   // [Img1 setImage:oImg];
    [self.view addSubview:Img1];
    
    CGRect f2=CGRectMake(120, 5, 80, 80);
    Img2 = [[UIImageView alloc]initWithFrame:f2];
    [Img2 setBounds:f2];
   // [Img2 setImage:xImg];
    [self.view addSubview:Img2];
    
    CGRect f3=CGRectMake(230, 5, 80, 80);
    Img3 = [[UIImageView alloc]initWithFrame:f3];
   // [Img2 setImage:oImg];
    [Img3 setBounds:f3];

    [self.view addSubview:Img3];
  
    //2 nd row
    
    CGRect f4=CGRectMake(10,120, 80, 80);
    Img4 = [[UIImageView alloc]initWithFrame:f4];
    //[Img2 setImage:xImg];
    [Img4 setBounds:f4];

    [self.view addSubview:Img4];
    
    CGRect f5=CGRectMake(120, 120, 80, 80);
    Img5 = [[UIImageView alloc]initWithFrame:f5];
    //[Img2 setImage:oImg];
    [Img5 setBounds:f5];

    [self.view addSubview:Img5];
    
    CGRect f6=CGRectMake(230, 120, 80, 80);
    Img6 = [[UIImageView alloc]initWithFrame:f6];
    //[Img2 setImage:oImg];
    [Img6 setBounds:f6];

    [self.view addSubview:Img6];

    //3rd row
    CGRect f7=CGRectMake(10, 235, 80, 80);
    Img7 = [[UIImageView alloc]initWithFrame:f7];
    //[Img2 setImage:oImg];
    [Img7 setBounds:f7];

    [self.view addSubview:Img7];

    CGRect f8=CGRectMake(120, 235, 80, 80);
    Img8 = [[UIImageView alloc]initWithFrame:f8];
    //[Img2 setImage:xImg];
    [Img8 setBounds:f8];

    [self.view addSubview:Img8];

    CGRect f9=CGRectMake(230, 235, 80, 80);
    Img9 = [[UIImageView alloc]initWithFrame:f9];
    [Img9 setBounds:f9];

    //[Img2 setImage:oImg];
    [self.view addSubview:Img9];

    player=1;

      ImgView = [[NSMutableDictionary alloc]init ];
      [ImgView setObject:Img1 forKey:@"1"];
      [ImgView setObject:Img2 forKey:@"2"];
      [ImgView setObject:Img3 forKey:@"3"];
      [ImgView setObject:Img4 forKey:@"4"];
      [ImgView setObject:Img5 forKey:@"5"];
      [ImgView setObject:Img6 forKey:@"6"];
      [ImgView setObject:Img7 forKey:@"7"];
      [ImgView setObject:Img8 forKey:@"8"];
      [ImgView setObject:Img9 forKey:@"9"];
   
   

    NewGameButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    NewGameButton.frame = CGRectMake(100, 350, 100, 40);
    [NewGameButton setTitle:@"New Game" forState:UIControlStateNormal];
    [NewGameButton addTarget:self action:@selector(NewGame:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:NewGameButton];
    
}
-(IBAction)NewGame:(id)sender
{
    [self viewDidLoad];
}
-(void)UpdatePlayer
{
    if(player==1)
        player=2;
    else if(player==2)
        player=1;
}
-(void)NextMove
{
    
  //  UIImageView *CurrentImg = [ImgView objectForKey:[NSString stringWithFormat:@"%d",placed]];
   
    if(placed %3 == 1)
    {
        UIImageView *CurrentCell = [ImgView objectForKey:[NSString stringWithFormat:@"%d",placed]];
        UIImageView *NeighbourCell = [ImgView objectForKey:[NSString stringWithFormat:@"%d",placed+1]];
        UIImageView *NextNeighbourCell = [ImgView objectForKey:[NSString stringWithFormat:@"%d",placed+2]];
           
        if(NeighbourCell.image ==xImg )
        {
            NextNeighbourCell.image=oImg;
        }
        else if (NextNeighbourCell.image == xImg)
        {
            NeighbourCell.image=oImg;
        }
      //  else if(
        
        
       /* UIImageView *ImgName;
        ImgName = [NSString stringWithFormat:@"Img%d",placed+1];
        ImgName.image=oImg;
    */
        }
    else if(placed %3 == 2)
    {
        
    }
    else if(placed %3 == 0)
    {
        
    }
}
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [[event allTouches] anyObject];
    
   
    
    if(CGRectContainsPoint([Img1 bounds], [touch locationInView:self.view])){
        if(player==1)
            Img1.image = xImg;
        if(player==2)
            Img1.image = oImg; 
        placed=1;
    }
    else if(CGRectContainsPoint([Img2 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img2.image = xImg;
        if(player==2)
            Img2.image = oImg; 
         placed=2;
    }
    else if(CGRectContainsPoint([Img3 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img3.image = xImg;
        if(player==2)
            Img3.image = oImg;
         placed=3;
    }
    else if(CGRectContainsPoint([Img4 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img4.image = xImg; 
        if(player==2)
            Img4.image = oImg;
         placed=4;
    }
    else if(CGRectContainsPoint([Img5 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img5.image = xImg; 
        if(player==2)
            Img5.image = oImg;
         placed=5;
    }
    else if(CGRectContainsPoint([Img6 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img6.image = xImg; 
        if(player==2)
            Img6.image = oImg;
         placed=6;
    }
    else if(CGRectContainsPoint([Img7 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img7.image = xImg; 
        if(player==2)
            Img7.image = oImg;
         placed=7;
    }
    else if(CGRectContainsPoint([Img8 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img8.image = xImg; 
        if(player==2)
            Img8.image = oImg;
         placed=8;
    }
    else if(CGRectContainsPoint([Img9 bounds], [touch locationInView:self.view]))
    {
        if(player==1)
            Img9.image = xImg; 
        if(player==2)
            Img9.image = oImg; 
         placed=9;
    }
    
    
    
     if([self IsSuccess] == YES)
     {
         if(player==1)
         {
             UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Congrats !!"
                                                             message:@"X Wins ! :)" delegate:self 
                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alertView show];
             [alertView release];
         }
         else
         {
             UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Congrats !!"
                                                                 message:@"O Wins ! :)" delegate:self 
                                                       cancelButtonTitle:@"OK" otherButtonTitles:nil];
             [alertView show];
             [alertView release];

         }
   
         
     }
    else
    {
       // [self NextMove];
    }
    [self UpdatePlayer];
   
    
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
          [self viewDidLoad];
    }

-(BOOL) IsSuccess
{
    if((Img1.image == Img2.image) && (Img2.image== Img3.image) && (Img3.image !=NULL))
        return YES;
    else if((Img4.image == Img5.image) && (Img5.image== Img6.image) && (Img6.image !=NULL))
        return YES;
    else if((Img7.image == Img8.image) && (Img8.image== Img9.image) && (Img9.image !=NULL))
        return YES;
    else if((Img1.image == Img4.image) && (Img4.image== Img7.image) && (Img7.image !=NULL))
        return YES;
    else if((Img2.image == Img5.image) && (Img5.image== Img8.image) && (Img8.image !=NULL))
        return YES;
    else if((Img3.image == Img6.image) && (Img6.image== Img9.image) && (Img9.image !=NULL))
        return YES;
    else if((Img1.image == Img5.image) && (Img5.image== Img9.image) && (Img9.image !=NULL))
        return YES;
    else if((Img3.image == Img5.image) && (Img5.image== Img7.image) && (Img7.image !=NULL))
        return YES;
    else
        return NO;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    [Img1 release];
    [Img2 release];
    [Img3 release];
    [Img4 release];
    [Img5 release];
    [Img6 release];
    [Img7 release];
    [Img8 release];
    [Img9 release];
    [NewGameButton release];
    [board release];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
