//
//  TicTacViewController.h
//  TicTac
//
//  Created by bijugopi on 12/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIKit/UIKit.h>

@interface TicTacViewController : UIViewController {
    
    IBOutlet UIImage *xImg;
    IBOutlet UIImage *oImg;
    
    IBOutlet UIImageView *Img1;
    IBOutlet UIImageView *Img2;
    IBOutlet UIImageView *Img3;
    IBOutlet UIImageView *Img4;
    IBOutlet UIImageView *Img5;
    IBOutlet UIImageView *Img6;
    IBOutlet UIImageView *Img7;
    IBOutlet UIImageView *Img8;
    IBOutlet UIImageView *Img9;
    
    UIImage *tictactoe;
    
    IBOutlet UIButton *NewGameButton;
    IBOutlet UIImageView *board;
       
    NSInteger player;
  //  NSMutableDictionary *ImgViewsByName;
    int placed;
    
 
}
@property int placed;
//@property  NSMutableDictionary *ImgViewsByName;
@property (nonatomic,retain)UIImage *xImg;
@property (nonatomic,retain)UIImage *oImg;

@property (nonatomic,retain)UIImage *tictactoe;
@property (nonatomic,retain)UIButton *NewGameButton;
@property (nonatomic,retain)UIImageView *board;

@property (nonatomic,retain)UIImageView *Img1;
@property (nonatomic,retain)UIImageView *Img2;
@property (nonatomic,retain)UIImageView *Img3;
@property (nonatomic,retain)UIImageView *Img4;
@property (nonatomic,retain)UIImageView *Img5;
@property (nonatomic,retain)UIImageView *Img6;
@property (nonatomic,retain)UIImageView *Img7;
@property (nonatomic,retain)UIImageView *Img8;
@property (nonatomic,retain)UIImageView *Img9;






-(void)UpdatePlayer;
-(BOOL)IsSuccess;
-(IBAction)NewGame:(id)sender;
-(void)NextMove;





@end

